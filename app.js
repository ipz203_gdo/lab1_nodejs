const user = require('./user');
let yargs = require('yargs');
let _ = require('lodash');

const argv = yargs.argv;

let method = _.first(_.map(argv));
method = method[0]

if (method == 'read') {
    if (_.isEmpty(argv.title))
        console.log("Команда read не працює без параметра назва")
    else
        user.read(argv.title)
} else if (method == 'list') {
    user.list()
} else if (method == 'add') {
    if (_.isEmpty(argv.title))
        console.log("Команда add не працює без параметра назва")
    else if (_.isEmpty(argv.level))
        console.log("Команда add не працює без параметра рівень")
    else
        user.add(argv.title, argv.level)
} else if (method == 'remove') {
    if (_.isEmpty(argv.title))
        console.log("Команда remove не працює без параметра назва")
    else
        user.remove(argv.title)
} else {
    console.log("Немає такої команди або немає такого методу")
}