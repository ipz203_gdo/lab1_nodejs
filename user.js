const fs = require("fs");
let _ = require('lodash');
//зчитування JSON
let data = JSON.parse(fs.readFileSync('./user.json'));

//вивід усього списку
function list() {
    if (data.languages.length == 0)
        console.log("Список мов порожній")
    else {
        _.forEach(data.languages, function (value) {
            console.log("Title: ", value.title, "\tlevel:", value.level)
        });
    }
}

//знаходження мови за назвою
function findLanguageByTitle(title) {
    let i = 0;
    let res = -1;
    _.forEach(data.languages, function (value) {
        if (value.title.toLowerCase() == title.toLowerCase()) {
            res = i;
        }
        i += 1;
    });
    return res;
}

//додавння мови
function add(title, level) {
    let index = findLanguageByTitle(title);
    if (index >= 0) {
        console.log("Вже є мова з назвою", title)
    } else {
        let language = {
            "title": title,
            "level": level
        }
        data.languages.push(language);
        fs.writeFile('user.json', JSON.stringify(data), (err) => {
            if (err) throw err;
        });
    }
}

//Видалення мови
function remove(title) {
    let index = findLanguageByTitle(title);
    if (index == -1) {
        console.log("Немає мови з такою назвою", title)
    } else {
        _.remove(data.languages, function (n) {
            return n.title.toLowerCase() == title.toLowerCase();
        });
        list()
        fs.writeFile('user.json', JSON.stringify(data), (err) => {
            if (err) throw err;
        });
    }
}

//Вивід мови
function read(title) {
    let index = findLanguageByTitle(title);
    if (index == -1) {
        console.log("Немає мови з назвою", title)
    } else {
        let value = data.languages[index]
        console.log("Title: ", value.title, "\tlevel:", value.level)
    }
}


module.exports = {
    list, add, remove, read
}