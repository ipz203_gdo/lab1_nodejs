let _ = require('lodash');
//1 Заношу всі елементи a в масив arr
let a = {id: 1, name: "some name"};
let arr = [];
_.each(a, function (item) {
    arr.push(item);
});
console.log(arr);
//2 Знаходимо два значення id
let newArr = _.map([{id: 1}, {id: 2}], 'id');
console.log(newArr);
//3 Прибираємо из масива всі пусті элементи (0, "", null, undefined)
console.log(_.compact([0, 1, false, 2, '', 3]));
//4 Повертає елементи до трьох не включно
console.log(_.filter([1, 2, 3, 4, 5], function (item) {
    return item < 3;
}));
//5 Повертає Unix time 
console.log(_.now());